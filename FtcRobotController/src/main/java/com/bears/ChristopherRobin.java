package com.bears;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.bears.Drive;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Awesomely created by Bears on 07/11/2015.
 */

public class ChristopherRobin extends OpMode{
    DcMotor rightFront;
    DcMotor rightBack;
    DcMotor leftFront;
    DcMotor leftBack;
    DcMotor espiropapa;
    DcMotor banda;

    Servo palanca1;
    Servo palanca2;

    Drive robotDrive;

    boolean servoChange = true;

    @Override
    public void init() {
        rightFront = hardwareMap.dcMotor.get("rightFront");
        rightBack = hardwareMap.dcMotor.get("rightBack");
        leftFront = hardwareMap.dcMotor.get("leftFront");
        leftBack = hardwareMap.dcMotor.get("leftBack");
        espiropapa = hardwareMap.dcMotor.get("espiropapa");
        banda = hardwareMap.dcMotor.get("banda");

        leftBack.setDirection(DcMotor.Direction.REVERSE);
        leftFront.setDirection(DcMotor.Direction.REVERSE);

        palanca1 = hardwareMap.servo.get("palanca1");
        palanca2 = hardwareMap.servo.get("palanca2");

        robotDrive = new Drive(rightFront, rightBack, leftFront, leftBack, gamepad1);
        robotDrive.setDeadband((float)0.15);
        robotDrive.setSmoothModeEnabled(true);
        robotDrive.setScaleModeEnabled(true);
    }

    @Override
    public void loop() {

        robotDrive.arcadeDrive();

        if(gamepad2.right_bumper){
            espiropapa.setPower(1);
        }
        else if (gamepad2.right_trigger > 0.1) {
            espiropapa.setPower(-1);
        }
        else{
            espiropapa.setPower(0);
        }


        if(gamepad2.left_bumper){
            banda.setPower(1);
        }
        else if (gamepad2.left_trigger > 0.1) {
            banda.setPower(-1);
        }
        else{
            banda.setPower(0);
        }


        if(gamepad2.start)
        {
            if(servoChange){
                if (palanca1.getPosition()==0.4){
                    palanca1.setPosition(.6);
                    palanca2.setPosition(.4);
                }
                else{
                    palanca1.setPosition(.4);
                    palanca2.setPosition(.6);
                }
                servoChange=false;
            }
        }
        else{
            servoChange=true;
        }


    }
}
